#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import SH1106
import time
import config
import traceback
import json, os, sys, logging
import subprocess

from cmd import Cmd
from button import Button

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import paho.mqtt.client as mqtt
from dataclasses import asdict

# step sizes
CMD_STEP_SIZE = 1
BRIGH_STEP_SIZE = 5

# Handle functions
def handle_left():
    str = extract_cmd_label(CMD_LIST[dec_cnt_cmd()])
    return str

def handle_right():
    str = extract_cmd_label(CMD_LIST[inc_cnt_cmd()])
    return str

def handle_up():
    str = send_bright_cmd(inc_cnt_bright())
    return str

def handle_down():
    str = send_bright_cmd(dec_cnt_bright())
    return str

def handle_key1():
    str = send_hotkey_cmd(1)
    return str

def handle_key2():
    str = send_hotkey_cmd(2)
    return str

def handle_key3():
    str = send_hotkey_cmd(3)
    return str

def handle_press():
    str = send_cnt_cmd()
    return str

def dec_cnt_cmd(val = CMD_STEP_SIZE):
    global CNT_CMD_IDX
    CNT_CMD_IDX = bound_value(CNT_CMD_IDX - val, len(CMD_LIST))
    return CNT_CMD_IDX

def inc_cnt_cmd(val = CMD_STEP_SIZE):
    global CNT_CMD_IDX
    CNT_CMD_IDX = bound_value(CNT_CMD_IDX + val, len(CMD_LIST))
    return CNT_CMD_IDX

def dec_cnt_bright(val = BRIGH_STEP_SIZE):
    global CNT_BRIGHT
    CNT_BRIGHT = bound_value(CNT_BRIGHT - val, MAX_BRIGHT)
    return CNT_BRIGHT

def inc_cnt_bright(val = BRIGH_STEP_SIZE):
    global CNT_BRIGHT
    CNT_BRIGHT = bound_value(CNT_BRIGHT + val, MAX_BRIGHT)
    return CNT_BRIGHT

def bound_value(val, bound):
    return val % bound

def send_cnt_cmd():
    global MQTT_CLIENT, MQTT_TOPIC
    global CMD_LIST, CNT_CMD_IDX

    cmd = CMD_LIST[CNT_CMD_IDX]

    cmd_str = json.dumps(asdict(cmd))

    MQTT_CLIENT.publish(MQTT_TOPIC, cmd_str, qos=1)

    return extract_cmd_label(cmd)

def send_bright_cmd(bright):
    global MQTT_CLIENT, MQTT_TOPIC
    global BRIGHT_CMD

    BRIGHT_CMD[PARAMS_KEY] = bright
    cmd_str = json.dumps(BRIGHT_CMD)

    MQTT_CLIENT.publish(MQTT_TOPIC, cmd_str, qos=1)

    return f"bright {bright}"

def send_hotkey_cmd(idx):
    global MQTT_CLIENT, MQTT_TOPIC
    global KEY_CMD_DICT

    cmd = KEY_CMD_DICT.get(idx)

    if not cmd:
        return

    cmd_str = json.dumps(asdict(cmd))

    MQTT_CLIENT.publish(MQTT_TOPIC, cmd_str, qos=1)

    return extract_cmd_label(cmd)

def extract_cmd_label(c):
    # early retrun if there is a tag
    if c.tag:
        return c.tag

    # fallback if no tag is set
    if not c.cmd == MODE_CMD_STRING:
        return c.cmd
    else:
        return c.params[MODE_KEY]

def on_connect(client, userdata, flags, rc):
    logging.info("CONN")

def on_disconnect(client, userdata, rc):
    logging.info("DISCO")
    MQTT_CLIENT.reconnect()

# TODO: Move to config file
RST_PIN = 25
CS_PIN = 8
DC_PIN = 24

KEY_UP_PIN = 6 
KEY_DOWN_PIN = 19
KEY_LEFT_PIN = 5
KEY_RIGHT_PIN = 26
KEY_PRESS_PIN = 13

KEY1_PIN = 21
KEY2_PIN = 20
KEY3_PIN = 16

# List with button pin and function
BTN_LIST = [
    (KEY_LEFT_PIN, handle_left),
    (KEY_RIGHT_PIN, handle_right),
    (KEY_UP_PIN, handle_up),
    (KEY_DOWN_PIN, handle_down),
    (KEY_PRESS_PIN, handle_press),
    (KEY1_PIN, handle_key1),
    (KEY2_PIN, handle_key2),
    (KEY3_PIN, handle_key3),
]

# change to the directory containing the script
os.chdir(sys.path[0])
# List with commands
CMD_LIST_FILE = "cmd_list.json"

# Currently selected command
CNT_CMD_IDX = 0
# current brightnes
CNT_BRIGHT = 50
## add BRIGHT_STEP_SIZE to the bound to allow 100 percent
MAX_BRIGHT = 100 + BRIGH_STEP_SIZE

BRIGHT_CMD = {
    "cmd": "BRIGHTNESS",
    "params": 42
}

CMD_KEY = "cmd"
PARAMS_KEY = "params"
MODE_KEY = "mode"
TAG_KEY = "tag"
KEY_BIND_KEY = "key_bind"
MODE_CMD_STRING = "MODE"

# read CMD_LIST_FILE
with open(CMD_LIST_FILE, "r") as f:
    CMD_LIST = [Cmd(i[CMD_KEY], i[PARAMS_KEY], i.get(TAG_KEY), i.get(KEY_BIND_KEY)) for i in json.load(f)]

# map hotkeys to commands
KEY_CMD_DICT = {i.key_bind: i for i in CMD_LIST if i.key_bind}

# List of button objects
BTN_HAL_LIST = [Button(p, f) for p,f in BTN_LIST]
# MQTT client
# TODO: Make it not global
MQTT_CLIENT = None

MQTT_BROKER = "<MQTT_BROKER>"
MQTT_TOPIC = "<MQTT_TOPIC>"

SLEEP_TIME_S = 0.01

def setup_mqtt(broker = MQTT_BROKER):
    """
    Create a MQTT client and connect to the given broker.

    Sets the global client MQTT_CLIENT. For now...
    """
    global MQTT_CLIENT

    # create client object
    MQTT_CLIENT = mqtt.Client()

    # set callbacks
    MQTT_CLIENT.on_disconnect = on_disconnect
    MQTT_CLIENT.on_connect = on_connect

    # connect to broker MQTT_BROKER
    MQTT_CLIENT.connect_async(broker)

    # loop client
    MQTT_CLIENT.loop_start()

def setup_disp():
    # Initialize library.
    DISP.Init()
    # Clear display.
    DISP.clear()

def show_string(s):
    image = Image.new('1', (DISP.width, DISP.height), 255)  # 255: clear the frame
    draw = ImageDraw.Draw(image)
    draw.text((5,0), s, font = FONT, fill = 0)
    DISP.ShowImage(DISP.getbuffer(image))

def setup_logging():
    # define log level
    logging.basicConfig(
        level=logging.INFO,
        format="%(levelname)s-%(message)s",
        )

# setup display
DISP = SH1106.SH1106()
FONT = ImageFont.truetype('Font.ttf', 20)
FONT10 = ImageFont.truetype('Font.ttf',13)

def main():

    setup_logging()
    setup_disp()
    show_string("Starting...")

    # setup mqtt
    setup_mqtt()

    while not MQTT_CLIENT.is_connected:
        pass

    show_string(extract_cmd_label(CMD_LIST[CNT_CMD_IDX]))

    while 1:
        for btn in BTN_HAL_LIST:
            ret = btn.handle_button()
            if ret:
                show_string(str(ret))
            time.sleep(SLEEP_TIME_S)

if __name__ == "__main__":
    main()