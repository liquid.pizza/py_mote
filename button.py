import logging
import RPi.GPIO as GPIO

# GPIO define
GPIO.setmode(GPIO.BCM)

class Button:
    NUM_LOOPS = 2
    
    """
    Simple Button class to handle button input.
    """
    def __init__(
        self,
        pin,
        fnct=None,
        num_loops=NUM_LOOPS,
        ):
        """
        Button constructor.
        Takes a pin number and an (optional) function pointer as parameters.
        """
        self.pin = pin
        self.fnct = fnct
        GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

        self._loop_counter = 0
        self._num_loops = num_loops

    def check_status(self):
        """
        Check if the button is pressed.
        Returns True if pressed False otherwise.

        Is inverted for now.
        TODO: Handle inverted logic with a flag
        """
        return not GPIO.input(self.pin)

    def handle_button(self):
        """
        Handle the button input.

        If the button is pressed increase the _loop_count.

        If the button is released check if the threshold (_num_loops) is exceeded and reset _loop_count.
        
        The function (fnct) is executet - if set - when the threshold is exceeded.
        """
        # button is pressed
        if self.check_status():
            self._loop_counter += 1
        # button is released
        else:
            is_triggerd = self._loop_counter >= self._num_loops
            self._loop_counter = 0
            # threshold exceeded
            if is_triggerd:
                logging.info(f"Button {self.pin} pressed")
                if self.fnct:
                    return self.fnct()
