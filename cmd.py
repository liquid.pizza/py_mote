from dataclasses import dataclass

@dataclass
class Cmd:
    cmd: str
    params: dict
    tag: str = ""
    key_bind: int = None
