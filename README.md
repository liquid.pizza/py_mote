# py_mote

[_Raspberry Pi Zero W_](https://www.raspberrypi.org/products/raspberry-pi-zero-w/) and [_Waveshare 1.3inch OLED HAT_](https://www.waveshare.com/wiki/1.3inch_OLED_HAT) based _python3_ hardware remote for lam_py.

## Headless Pi Setup

1. Flash mircro SD card with [Raspberry Pi Os](https://www.raspberrypi.org/software/operating-systems/#raspberry-pi-os-32-bit)
1. Setup headless
    1. Place file `ssh` in `/` on the partition `boot`
    1. Create file `wpa_supplicant.conf` in `/` on the partition `boot` containing:

    ```bash
    country=DE
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1
    network={
        ssid="WLAN SSID"
        scan_ssid=1
        psk="WLAN PASSWORT"
        key_mgmt=WPA-PSK
    }
    ```

The pi is read to boot.

*Source:* https://www.dahlen.org/2017/10/raspberry-pi-zero-w-headless-setup/

## Install

Clone the repo:

```sh
git clone https://gitlab.com/liquid.pizza/py_mote.git
```

Install `paho-mqtt`:

```sh
pip3 install paho-mqtt
```

Run the `main.py`:

```sh
python3 main.py
```

## Run at boot up

Make sure `main.py` is executable:

```sh
chmod +x main.py
```

Edit the cron jobs:

```sh
crontab -e

```

Add the line at the bottom - remember to change `<path_to_dir>` to the project directory:

```cron
@reboot python3 <path_to_dir>/main.py
```
